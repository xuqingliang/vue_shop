import Vue from 'vue'
import Router from 'vue-router'
import Login from './components/Login.vue'
import Home from './components/Home.vue'
import Welcome from './components/Welcome.vue'
import User from './components/User.vue'
import Rights from './components/power/Rights'
import Roles from './components/power/Roles'
Vue.use(Router)

const router = new Router({
  routes: [{
      path: "/",
      redirect: '/login'
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/home',
      component: Home,
      redirect: '/welcome',
      children: [{
        path: '/welcome',
        component: Welcome
      },
      {
        path: '/users',
        component: User
      },
      {
        path:'/rights',
        component:Rights
      },
      {
        path:'/roles',
        component:Roles
      }
    ]
    }
  ]
})
// 路由导航守卫
router.beforeEach((to, from, next) => {
  //to 将要访问的路径
  //from代表从哪里路径条转而来
  // next 是一个函数，表示放行
  //     next()  放行    next('/login')  强制跳转


  // 访问登录页
  if (to.path === '/login') return next()
  const tokenStr = window.sessionStorage.getItem('token')
  // 判断有没有登录
  if (!tokenStr) return next('login')
  next()
})
export default router
